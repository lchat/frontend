import { Injectable } from '@angular/core';
import { CanActivate, UrlTree, Router } from '@angular/router';

export function getLoggedInUserId() {
  return localStorage.getItem(AuthGuard.KEY_LOGGED_IN_USER_ID);
}


@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  public static readonly KEY_LOGGED_IN_USER_ID = 'logged_in_user_id';

  constructor(private router: Router) {}

  canActivate(): boolean | UrlTree {
    const userid = getLoggedInUserId();
    return userid !== null && userid.length > 0 ? true : this.router.parseUrl('/login');
  }
}
