import { Component, OnInit, HostListener, OnDestroy, ChangeDetectionStrategy } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { CheckMessages, PostMessage } from '../api.actions';
import { Subscription, Observable, timer } from 'rxjs';
import { switchMap, filter, map, tap, delay } from 'rxjs/operators';
import { FormControl, Validators } from '@angular/forms';
import { ApiStateModel, ApiState } from '../api.state';
import { getLoggedInUserId } from '../auth.guard';

@Component({
  selector: 'lchat-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChatComponent implements OnInit, OnDestroy {
  @Select(ApiState) api: Observable<ApiStateModel>;
  readonly currentUserid = getLoggedInUserId();
  message = new FormControl('', Validators.required);

  private subscriptions: Subscription[] = [];

  constructor(private store: Store) { }

  ngOnInit(): void {
    // subscripe on new messages
    const messagesList = document.getElementsByClassName('messages-wrapper')[0];
    this.subscriptions.push(this.api.pipe(
        filter(apiState => apiState.checkMessagesSuccess),
        // this should be called before the list gets updated...
        map(() => Math.ceil(messagesList.scrollHeight - messagesList.scrollTop) === messagesList.clientHeight),
        filter(wasScrolledToBottom => wasScrolledToBottom),
        delay(500))
      .subscribe(() => messagesList.scrollBy({top: messagesList.scrollHeight, behavior: 'smooth'})));
   
    // poll for new messages repeatedly 
    this.subscriptions.push(timer(0, 3000)
      .pipe(switchMap(() => this.store.dispatch(new CheckMessages())))
      .subscribe());
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
    this.subscriptions = [];
  }

  // send message with ENTER
  @HostListener('document:keydown', ['$event'])
  public handleKeyboardEvent(event: KeyboardEvent): void {
    if (event.keyCode === 13) {
      this.onSendMessage();
    }
  }

  onSendMessage(): void {
    if (this.message.valid) {
      this.store.dispatch(new PostMessage(this.message.value))
        .pipe(delay(200),
          switchMap(() => this.store.dispatch(new CheckMessages())))
        .subscribe(() => this.message.reset());
    } else {
      this.message.markAsDirty();
    }
  }
}
