export class Login {
    static readonly type = '[API] Login';
    constructor(public readonly username: string, public readonly password: string) {}
}

export class Logout {
    static readonly type = '[API] Logout';
    constructor() {}
}

export class CheckMessages {
    static readonly type = '[API] Check Messages';
    constructor() {}
}

export class PostMessage {
    static readonly type = '[API] Post Message';
    constructor(public readonly message: string) {}
}
