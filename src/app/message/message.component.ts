import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { Message } from '../message.model';
import * as emoji from 'node-emoji-new';

@Component({
  selector: 'lchat-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MessageComponent implements OnInit {
  @Input() message: Message;
  @Input() isMyMessage = false;
  textWithEmojis: string;
  avatarUrl: string;

  constructor() { }

  ngOnInit(): void {
    this.textWithEmojis = emoji.emojify(this.message.message);
    this.avatarUrl = `https://images.unsplash.com/${this.message.avatar}?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=200&fit=max`;
  }
}
