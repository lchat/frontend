import { Component, OnInit } from '@angular/core';
import { Observable, BehaviorSubject, timer } from 'rxjs';
import { filter } from 'rxjs/operators';
import { Select, Store } from '@ngxs/store';
import { ApiState, ApiStateModel } from './api.state';
import { Logout } from './api.actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  showAlert: BehaviorSubject<boolean> = new BehaviorSubject(false);
  @Select(ApiState) api: Observable<ApiStateModel>;

  constructor(private store: Store) {}

  ngOnInit() {
    this.api
      .pipe(filter(apiState => apiState.hasError))  
      .subscribe(() => this.showErrorMsg());
  }

  onLogout(): void {
    this.store.dispatch(new Logout()).subscribe();
  }

  private showErrorMsg(): void {
    this.showAlert.next(true);
    timer(4000).subscribe(() => this.showAlert.next(false));
  }
}
