import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Message } from './message.model';
import { tap, map, mapTo, filter } from 'rxjs/operators';
import { AuthGuard, getLoggedInUserId } from './auth.guard';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private readonly http: HttpClient) { }

  public login(username: string, password: string): Observable<void> {
    return this.http.post(`${environment.apiBaseUrl}/login`, {username, password})
        .pipe(tap((json: any) => localStorage.setItem(AuthGuard.KEY_LOGGED_IN_USER_ID, json.userid)),
          mapTo(undefined));
  }

  public logout(): Observable<void> {
    return of(undefined).pipe(tap(() => localStorage.removeItem(AuthGuard.KEY_LOGGED_IN_USER_ID)));
  }

  public checkMessages(lastTimestamp: Date): Observable<Message[]> {
    return this.http.get(`${environment.apiBaseUrl}/message?from=${lastTimestamp.toISOString()}`, this.getOptions())
      .pipe(map((json: any) => json.messages),
        filter(messages => !!messages),
        map(messages => messages.map(Message.parse)));
  }

  public postMessage(message: string): Observable<void> {
    return this.http.post(`${environment.apiBaseUrl}/message`, {message}, this.getOptions())
        .pipe(mapTo(undefined));
  }

  
  private getOptions(): {} {
    return {
        headers: {
            // not exactly OAuth but it does what its supposed to
            'X-USERID': getLoggedInUserId()
        }
    };
}
}
