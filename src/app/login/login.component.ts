import { Component, ChangeDetectionStrategy } from '@angular/core';
import { Store } from '@ngxs/store';
import { Login } from '../api.actions';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'lchat-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginComponent {
  loginForm = new FormGroup({
    username: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required)
  });

  constructor(private store: Store) { }

  onLogin(): void {
    if (this.loginForm.valid) {
      this.store.dispatch(new Login(this.loginForm.get('username').value, this.loginForm.get('password').value))
        .pipe(filter(apiState => apiState.loggedIn))
        .subscribe(() => this.loginForm.reset());
    }
  }

  isFormControlValid(name: string): boolean {
    const formControl = this.loginForm.get(name);
    return formControl.pristine || formControl.valid;
  }
}
