export class Message {

    public static parse(json: any): Message {
        // dates are sent without timezone - expect UTC
        return new Message(new Date(`${json.timestamp}Z`), json.userid, json.username, json.useravatar, json.message);
    }

    constructor(public readonly timestamp: Date,
        public readonly userid: string,
        public readonly username: string,
        public readonly avatar: string,
        public readonly message: string) {}
}
