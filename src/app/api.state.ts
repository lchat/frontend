import { State, Action, StateContext } from '@ngxs/store';
import { Injectable } from '@angular/core';
import { Login, CheckMessages, PostMessage, Logout } from './api.actions';
import { ApiService } from './api.service';
import { Message } from './message.model';
import { map, switchMap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { from } from 'rxjs';
import { AuthGuard, getLoggedInUserId } from './auth.guard';

export interface ApiStateModel {
    hasError: boolean;
    loggedIn: boolean;
    checkMessagesSuccess: boolean;
    postMessageSuccess: boolean;
    lastTimestamp: Date,
    messages: Message[];
}

@State<ApiStateModel>({
    name: 'api',
    defaults: {
        hasError: false,
        loggedIn: !!getLoggedInUserId(),
        checkMessagesSuccess: undefined,
        postMessageSuccess: undefined,
        lastTimestamp: ApiState.getFirstTimestamp(),
        messages: []
    }
})
@Injectable()
export class ApiState {

    constructor(private apiService: ApiService, private router: Router) {}

    @Action(Login)
    doLogin(ctx: StateContext<ApiStateModel>, action: Login) {
        const state = ctx.getState();
        this.apiService.login(action.username, action.password)
                .pipe(switchMap(() => from(this.router.navigate(['/chat']))))
            .subscribe(() => ctx.setState({...state, hasError: false, loggedIn: true}),
                () => ctx.setState({...state, hasError: true, loggedIn: false}));
    }

    @Action(Logout)
    doLogout(ctx: StateContext<ApiStateModel>) {
        const state = ctx.getState();
        this.apiService.logout()
                .pipe(switchMap(() => from(this.router.navigate(['/']))))
            .subscribe(() => ctx.setState({...state, hasError: false, loggedIn: false}),
                () => ctx.setState({...state, hasError: true}));
    }

    @Action(CheckMessages)
    doCheckMessages(ctx: StateContext<ApiStateModel>) {
        const state = ctx.getState();
        this.apiService.checkMessages(state.lastTimestamp)
            .pipe(map((newMessages: Message[]) => newMessages.sort((a, b) => a.timestamp.getTime() - b.timestamp.getTime())),
                map(messages => [...state.messages, ...messages]))
            .subscribe(messages => ctx.setState({
                ...state,
                hasError: false,
                checkMessagesSuccess: true,
                lastTimestamp: messages.length > 0 ? messages[messages.length - 1].timestamp : state.lastTimestamp,
                messages: messages
            }),
                () => ctx.setState({...state, hasError: true, checkMessagesSuccess: false}));
    }

    @Action(PostMessage)
    doPostMessage(ctx: StateContext<ApiStateModel>, action: PostMessage) {
        const state = ctx.getState();
        this.apiService.postMessage(action.message)
            .subscribe(() => ctx.setState({...state, hasError: false, postMessageSuccess: true}),
                () => ctx.setState({...state, hasError: true, postMessageSuccess:false}));
    }

    // load initally all messages from up to one year ago
    private static getFirstTimestamp(): Date {
        const firstTimestamp = new Date();
        firstTimestamp.setFullYear(firstTimestamp.getFullYear() - 1);
        return firstTimestamp;
    }
}
